package arbolesBinarios;

import java.io.*;

public class BinaryTree
{
  private Node root;
  
  public BinaryTree(int data) { this.root = Node.basic_build(data); }

  public void insert(int ...data)
  {
    for(int i : data)
    {
      this.insert(i);
    }
  }

  public void insert(int data)
  {
    Node node = Node.build_node(data, this.root);
    this.root.insert(node);
  }

  public boolean lookup(int data)
  {
    Node result = Node.lookup(this.root, data);
    return (result != null);
  }
  
  public int max_depth()
  {
    if(this.root.is_leaf()) return 0;
    return Node.max_depth(this.root);
  }

  public int min_value() { return Node.min_value(this.root); }

  public int max_value() { return Node.max_value(this.root); }

  public void print_tree()
  {
    Node.print_tree(this.root);
    System.out.println();
  }

  public void print_postorder()
  {
    Node.print_postorder(this.root);
    System.out.println();
  }

  public boolean has_path_sum(int sum) { return Node.has_path_sum(this.root, sum); }

  public void print_paths()
  {
    int[] path = new int[1000];
    Node.print_paths(this.root, path, 0);
  }

  public void mirror() { Node.mirror(this.root); }

  public void doubleTree() { Node.doubleTree(this.root); }

  public boolean sameTree(BinaryTree tree) { return Node.same_tree(this.root, tree.root); }

  public boolean is_bst() { return Node.is_bst_1(this.root); }

  public boolean is_bst_2() { return Node.is_bst_2(this.root, Integer.MIN_VALUE, Integer.MAX_VALUE); }

  public int size() { return this.root.size(this.root); }
  
  public void build123() { this.root = Node.build_123(this.root); }

  public int count_trees(int i) { return Node.count_trees(i); }

  public void save_on_disk(String path) throws Exception
  {
    File f = new File(path);
    if(f.isDirectory()) throw new Exception("The path is not a file");

    ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
    out.writeObject(this.root);
    out.flush();
    out.close();
  }

  public void load_from_disk(String path) throws Exception
  {
    File f = new File(path);
    if(!f.exists()) throw new Exception("The file provided doesn't exist");
    if(f.isDirectory()) throw  new Exception("The file is a directory");

    ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
    this.root = (Node)in.readObject();
    in.close();
  }
}

class Node implements Serializable, Cloneable
{
  private Node left, right, root;
  private int data;
 
  private Node() {}
  
  public static Node build_node(int data, Node root)
  {
    Node node = Node.basic_build(data);
    node.root = root;
    return node;
  }
  
  public static Node build_123(Node root)
  {
    Node node = Node.basic_build(2);
    node.insert(Node.build_node(1, root));
    node.insert(Node.build_node(3, root));
    return node;
  }
  
  public static Node basic_build(int data)
  {
    Node node = new Node();
    node.left = null;
    node.right = null;
    node.root = null;
    node.data = data;
    return node;
  }

  public static Node lookup(Node root, int data)
  {
    if(root == null) return null;
    if(root.data == data) return root;

    Node tmp = null;
    if(data <= root.data) tmp = Node.lookup(root.left, data);
    else tmp = Node.lookup(root.right, data);

    return tmp;
  }
  
  public static int max_depth(Node root)
  {
    if(root.is_leaf()) return 1;

    int left_depth = 0, right_depth = 0;

    if(root.left != null) left_depth += Node.max_depth(root.left);
    if(root.right != null) right_depth += Node.max_depth(root.right);

    if(left_depth > right_depth)
      return left_depth + 1;
    else
      return right_depth + 1;
  }

  public static int min_value(Node root)
  {
    if(root.left == null)
      return root.data;
    else
      return Node.min_value(root.left);
  }

  public static int max_value(Node root)
  {
    if(root.right == null)
      return root.data;
    else
      return Node.max_value(root.right);
  }

  public static void print_tree(Node root)
  {
    if(root.left != null) Node.print_tree(root.left);
    System.out.print(" " + root.data);
    if(root.right != null) Node.print_tree(root.right);
  }

  public static void print_paths(Node node, int [] path, int pathLen)
  {
    if(node == null) return;
    path[pathLen] = node.data;
    pathLen++;

    if( node.left == null && node.right == null)
      Node.print_array(path, pathLen);
    else
    {
      Node.print_paths(node.left, path, pathLen);
      Node.print_paths(node.right, path, pathLen);
    }
  }

  private static void print_array(int[] ints, int len)
  {
    int i;
    for ( i = 0; i < len; i++) System.out.print(ints[i] + " ");
    System.out.println();
  }

  public static void print_postorder(Node root)
  {
    if(root.left != null) Node.print_postorder(root.left);
    if(root.right != null) Node.print_postorder(root.right);

    if(root.left != null) System.out.print(" " + root.left.data);
    if(root.right != null) System.out.print(" " + root.right.data);

    if(root.root == null) System.out.print(" " + root.data);
  }

  /**
   * TODO: Finish this mutherfucking shit.
   */
  public static boolean has_path_sum(Node root, Integer sum)
  {
    if(root == null || sum == 0)
      return true;
    else
    {
      int sub = sum - root.data;
      return has_path_sum(root.left, sub) || has_path_sum(root.right, sub);
    }
  }

  public static void mirror(Node root)
  {
    Node tmp_r;

    if(root.right == null && root.left != null) {root.right = root.left; root.left = null;}
    if(root.left == null && root.right != null) {root.left = root.right; root.right = null;}
    if(root.right != null && root.left != null)
    {
      Node tmp = root.left;
      root.left = root.right;
      root.right = tmp;
    }

    if(root.right != null) Node.mirror(root.right);
    if(root.left != null) Node.mirror(root.left);
  }

  public static void doubleTree(Node root)
  {
    if(root == null) return;

    Node.doubleTree(root.left);
    Node.doubleTree(root.right);

    Node lft = root.left;
    root.left = Node.basic_build(root.data);
    root.left.left = lft;
  }

  public static boolean same_tree(Node a, Node b)
  {
    if(a == null && b == null) { return true; }
    else if(a != null && b != null)
    {
      return
         (
          a.data == b.data &&
          Node.same_tree(a.left, b.left) &&
          Node.same_tree(b.right, b.right)
         );
    }
    else { return false; }
  }

  public static int count_trees(int i)
  {
    if(i <= 1) return 1;
    else
    {
      int sum = 0;
      int left, right, root;

      for(root = 1; root <= i; root++)
      {
        left = Node.count_trees(root - 1);
        right = Node.count_trees(i - root);
        sum += left * right;
      }

      return sum;
    }
  }

  public static boolean is_bst_1(Node node)
  {
    if(node == null) return true;

    if (node.left!=null && Node.max_value(node.left) > node.data) return(false);
    if (node.right!=null && Node.min_value(node.right) <= node.data) return(false);

    return( Node.is_bst_1(node.left) && Node.is_bst_1(node.right) );
  }

  public static boolean is_bst_2(Node node, int min, int max)
  {
    if(node == null) return true;
    else
    {
      boolean left_ok = Node.is_bst_2(node.left, min, node.data);
      if(!left_ok) return false;
      boolean right_ok = Node.is_bst_2(node.right, node.data + 1, max);
      return right_ok;
    }
  }

  public int size(Node root)
  {
    if(root.left == null && root.right == null) return 1;
    
    int result = 1;
    
    if(root.left != null) result += root.left.size(root.left);
    if(root.right != null) result += root.right.size(root.right);
    
    return result;
  }
  
  public void insert(Node node)
  { 
    if(node.data <= this.data)
    {
      if(this.left == null)
        this.left = node;
      else
        this.left.insert(node);
    }
    else
    {
      if(this.right == null)
        this.right = node;
      else
        this.right.insert(node);
    }
  }

  public boolean is_leaf()
  {
      return this.left == null && this.right == null;
  }
}