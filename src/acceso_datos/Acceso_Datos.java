package acceso_datos;

import arbolesBinarios.BinaryTree;

public class Acceso_Datos
{
  public static void main(String[] args)
  {
    BinaryTree tree = new BinaryTree(4);
    tree.insert(5,6,5,3,1);
    do_stuff(tree);
/*
    Throws Exception!
    tree.save_on_disk("/path/to/file");
    tree.load_from_disk("/path/to/file");
*/
  }

  public static void do_stuff(BinaryTree tree)
  {
    System.out.println("Lookup (4): " + tree.lookup(4));
    System.out.println("Has path sum (3): " + tree.has_path_sum(3));
    System.out.println("Max value: " + tree.max_value());
    System.out.println("Min value: " + tree.min_value());
    System.out.println("Size: " + tree.size());
    System.out.println("Max depth: " + tree.max_depth());
    System.out.println("Is binary search tree: " + tree.is_bst());
    System.out.println("Count trees: " + tree.count_trees(tree.size()));
    System.out.println("\nPaths: ");
    tree.print_paths();
    System.out.println("\nPrinted tree: ");
    tree.print_tree();
    System.out.println("\nPrinted tree postorder:");
    tree.print_postorder();
  }
}
